from django.apps import AppConfig


class PeoplecountConfig(AppConfig):
    name = 'peoplecount'
